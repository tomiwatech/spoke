import 'mocha'
import { expect } from 'chai';
import { TTask } from '../../src/types/task'
import { addTask, getTasks, fetchTaskById, editTask, deleteTaskById } from '../../src/services/task.service'

describe('Task Service', () => {
    it('should add a task', () => {
        const task: TTask = {
            id: 10,
            name: "task1",
            status: "progress",
            duration: 1,
            category: "work",
            point: 3
        }
       const response = addTask(task) 
       expect(response.code).to.equal(201);
       expect(response.message).to.equal('New Task added successfully');
       expect(response.status).to.equal('success');
       expect(response.data).to.equal(null)
    });

    it('should get all tasks', () => {
       const response = getTasks() 
       expect(response.code).to.equal(200);
       expect(response.message).to.equal('Tasks fetched successfully');
       expect(response.status).to.equal('success');
       expect(response.data).to.be.an('array')
    });

    it('should get task by id ', () => {
        const response = fetchTaskById(10) 
        expect(response.code).to.equal(200);
        expect(response.message).to.equal('Task fetched successfully');
        expect(response.status).to.equal('success');
        expect(response.data).to.be.an('object')
     });

     it('should edit a task by id ', () => {
        const task: TTask = {
            id: 10,
            name: "completed task",
            status: "completed",
            duration: 1,
            category: "work",
            point: 3
        }
        const response = editTask(10, task) 
        expect(response.code).to.equal(200);
        expect(response.message).to.equal('Task updated successfully');
        expect(response.status).to.equal('success');
        expect(response.data).to.be.an('array')
     });

     it('should add another task', () => {
        const task: TTask = {
            id: 11,
            name: "task2",
            status: "todo",
            duration: 1,
            category: "work",
            point: 3
        }
       const response = addTask(task) 
       expect(response.code).to.equal(201);
       expect(response.message).to.equal('New Task added successfully');
       expect(response.status).to.equal('success');
       expect(response.data).to.equal(null)
    });

    it('should delete task with id', () => {
       const response = deleteTaskById(11) 
       expect(response.code).to.equal(200);
       expect(response.message).to.equal('Task deleted successfully');
       expect(response.status).to.equal('success');
       expect(response.data).to.be.an("array")
       expect(response.data.length).to.equal(1)
    });

});