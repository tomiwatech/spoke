import 'mocha'
import { expect } from 'chai';
import { agent as request } from 'supertest';
import { app } from '../../src/index';

describe('Endpoint - api/v1/tasks', () => {

    it('should try to to add a task without name and status', async () => {
        const task = {
            id: 1,
            duration: 1,
            category: "work",
            point: 3
        }

        const res = await request(app).post('/api/v1/tasks').send(task);
        expect(res.body.errorType).to.equal('Validation');
        expect(res.body.errorMessage).to.equal('Task validation error');
        expect(res.body.errorsValidation[0].name).to.equal('Task name is required');
        expect(res.body.errorsValidation[1].status).to.equal('Task status is required');
    });

    it('should try to to add new tasks', async () => {

        const task = {
            id: 1,
            name: "task1",
            status: "progress",
            duration: 1,
            category: "work",
            point: 3
        }

        const res = await request(app).post('/api/v1/tasks').send(task);
        expect(res.body.code).to.equal(201);
        expect(res.body.message).to.equal('New Task added successfully');
        expect(res.body.status).to.equal('success');
        expect(res.body.data).to.equal(null)
    });

    it('should try to to add task with already existing name', async () => {
        const task = {
            id: 1,
            name: "task1",
            status: "progress",
            duration: 1,
            category: "work",
            point: 3
        }

        const res = await request(app).post('/api/v1/tasks').send(task);
        expect(res.body.errorRaw.stack.indexOf('Error: Task with name, already exists')).to.be.greaterThanOrEqual(0)
        expect(res.body.errorType).to.equal("Raw");
        expect(res.body.errorMessage).to.equal("Error");
    });

    it('should add a new task with another name', async () => {

        const task = {
            id: 2,
            name: "task2",
            status: "completed",
            duration: 1,
            category: "work",
            point: 3
        }

        const res = await request(app).post('/api/v1/tasks').send(task);
        expect(res.body.code).to.equal(201);
        expect(res.body.message).to.equal('New Task added successfully');
        expect(res.body.status).to.equal('success');
        expect(res.body.data).to.equal(null)
    });

    it('should get all tasks', async () => {
        const res = await request(app).get('/api/v1/tasks');
        expect(res.body.message).to.equal("Tasks fetched successfully");
        expect(res.body.code).to.equal(200);
        expect(res.body.status).to.equal("success");
        expect(res.body.data).to.be.an("array");
    });

    it('should get task with id that does not exists', async () => {
        const res = await request(app).get(`/api/v1/tasks/${100}`);
        expect(res.body.errorRaw.stack.indexOf('Error: Task with id, not found')).to.be.greaterThanOrEqual(0)
        expect(res.body.errorType).to.equal("Raw");
        expect(res.body.errorMessage).to.equal("Error");
    });

    it('should get task by id', async () => {
        const res = await request(app).get(`/api/v1/tasks/${2}`);
        expect(res.body.message).to.equal("Task fetched successfully");
        expect(res.body.code).to.equal(200);
        expect(res.body.status).to.equal("success");
        expect(res.body.data).to.be.an("object");
        expect(res.body.data.id).to.equal(2);
        expect(res.body.data.status).to.equal("completed");
        expect(res.body.data.category).to.equal("work");
    });

    it('should ensure task id is a number', async () => {
        const res = await request(app).get(`/api/v1/tasks/aaaaa`);
        expect(res.body.errorType).to.equal('Validation');
        expect(res.body.errorMessage).to.equal('Task validation error');
        expect(res.body.errorsValidation[0].id).to.equal('Task id must be a number');
    });

    it('should not be able to edit task without name and status', async () => {
        const task = {
            id: 2,
            duration: 1,
            category: "work",
            point: 3
        }

        const res = await request(app).put('/api/v1/tasks/2').send(task);
        expect(res.body.errorType).to.equal('Validation');
        expect(res.body.errorMessage).to.equal('Task update validation error');
        expect(res.body.errorsValidation[0].name).to.equal('Task name is required');
        expect(res.body.errorsValidation[1].status).to.equal('Task status is required');
    });

    it('should be able to edit task with correct id', async () => {
        const task = {
            id: 2,
            name: "edited task 1",
            status: "completed",
            duration: 1,
            category: "work",
            point: 5
        }

        const res = await request(app).put(`/api/v1/tasks/${2}`).send(task);
        expect(res.body.message).to.equal('Task updated successfully');
        expect(res.body.code).to.equal(200);
        expect(res.body.status).to.equal("success");
        expect(res.body.data).to.be.an("array");
        expect(res.body.data[1].name).to.equal("edited task 1");
        expect(res.body.data[1].status).to.equal("completed");
    });

    it('should not be able to edit task with wrong id', async () => {
        const task = {
            id: 2,
            name: "edited task 1",
            status: "completed",
            duration: 1,
            category: "work",
            point: 5
        }

        const res = await request(app).put(`/api/v1/tasks/${1000}`).send(task);
        expect(res.body.errorRaw.stack.indexOf('Error: Task with id, not found')).to.be.greaterThanOrEqual(0)
        expect(res.body.errorType).to.equal("Raw");
        expect(res.body.errorMessage).to.equal("Error");
    });

    it('should add a third task', async () => {

        const task = {
            id: 3,
            name: "task3",
            status: "todo",
            duration: 1,
            category: "work",
            point: 3
        }

        const res = await request(app).post('/api/v1/tasks').send(task);
        expect(res.body.code).to.equal(201);
        expect(res.body.message).to.equal('New Task added successfully');
        expect(res.body.status).to.equal('success');
        expect(res.body.data).to.equal(null)
    });

    it('should not be able to delete task with wrong id', async () => {
        const res = await request(app).delete(`/api/v1/tasks/${1000}`)
        expect(res.body.errorRaw.stack.indexOf('Error: Task with id, not found')).to.be.greaterThanOrEqual(0)
        expect(res.body.errorType).to.equal("Raw");
        expect(res.body.errorMessage).to.equal("Error");
    });

    it('should be able to delete the third task with correct id', async () => {
        const res = await request(app).delete(`/api/v1/tasks/3`)
        expect(res.body.code).to.equal(200);
        expect(res.body.message).to.equal('Task deleted successfully');
        expect(res.body.status).to.equal('success');
        expect(res.body.data).to.be.an("array")
        expect(res.body.data.length).to.equal(2)
    });
});