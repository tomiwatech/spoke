## Spoke

A simple api to add,delete, update and fetch tasks

---

## Install

    $ git clone https://gitlab.com/tomiwatech/spoke.git/
    $ cd spoke
    $ npm install

## Configure app

create a `.env` file then add the following values.

```
SPOKE_APP_PORT=9005

SPOKE_TEST_PORT=9006
NODE_ENV=development

```
## Running the project - development

    $ npm run dev 

## Running the project - production

    $ npm run start 

## Building the project

    $ npm run build 
     
## Running Tests

Integration Tests
    
    $ npm run test-integration 

Unit Tests
    
    $ npm run test-unit 
## Documentation

[Postman Collection](https://documenter.getpostman.com/view/3064040/UVJWs1eL)

## API

The API is hosted [here](https://spoke.softsignatureslab.com/)

