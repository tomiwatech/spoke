import dotenv from 'dotenv';

dotenv.config();
const test = {
    APP_PORT: process.env.SPOKE_TEST_PORT,
};

export default test;
