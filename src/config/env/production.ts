import dotenv from 'dotenv';

dotenv.config();
const production = {
    APP_PORT: process.env.SPOKE_APP_PORT,
};

export default production;
