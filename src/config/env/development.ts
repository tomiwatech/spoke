import dotenv from 'dotenv';

dotenv.config();
const development = {
    APP_PORT: process.env.SPOKE_APP_PORT,
};

export default development;
