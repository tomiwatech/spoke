import { Request, Response, NextFunction } from 'express';

import { CustomError } from 'utils/response/custom-error/customer-error';
import { ErrorValidation } from 'utils/response/custom-error/types';

export const validateNewTask = (req: Request, res: Response, next: NextFunction) => {
    let { name, status, duration, category, point } = req.body;

    const errorsValidation: ErrorValidation[] = [];

    if (!name ) {
        errorsValidation.push({ name: 'Task name is required' });
    }

    if(!status){
        errorsValidation.push({ status: 'Task status is required' });
    }

    const allowedStatusList = ["todo", "progress", "completed"]

    if(status && allowedStatusList.indexOf(status) < 0){
        errorsValidation.push({ status: 'Allowed status includes  [todo, progress, completed]' });
    }

    if (!duration) {
        errorsValidation.push({ duration: 'Task duration is required' });
    }

    if (!point) {
        errorsValidation.push({ point: 'Task point is required' });
    }

    if (!category) {
        errorsValidation.push({ category: 'Task category is required' });
    }


    if (errorsValidation.length > 0) {
        const customError = new CustomError(400, 'Validation', 'Task validation error', null, null, errorsValidation);
        return next(customError);
    }
    return next();
};

export const validateParam = (req: Request, res: Response, next: NextFunction) => {
    let { id } = req.params;

    const errorsValidation: ErrorValidation[] = [];

    if (!(+id)) {
        errorsValidation.push({ id: 'Task id must be a number' });
    }

    if (errorsValidation.length > 0) {
        const customError = new CustomError(400, 'Validation', 'Task validation error', null, null, errorsValidation);
        return next(customError);
    }
    return next();
};

export const validateUpdateNewTask = (req: Request, res: Response, next: NextFunction) => {
    let { name, status } = req.body;
  
    const errorsValidation: ErrorValidation[] = [];
  
    if (!name) {
      errorsValidation.push({ name: 'Task name is required' });
    }
  
    if (!status) {
      errorsValidation.push({ status: 'Task status is required' });
    }  
  
    if (errorsValidation.length > 0) {
      const customError = new CustomError(400, 'Validation', 'Task update validation error', null, null, errorsValidation);
      return next(customError);
    }
    return next();
  };