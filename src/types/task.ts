
export type TTask = {
    id: number
    status: string
    name: string
    duration: number
    category: string
    point: number
}