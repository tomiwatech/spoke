import { TTask } from "types/task";

let tasks: TTask[] = []

const generateTaskId = (min: number, max: number) => {
    return Math.floor(Math.random() * (max - min + 1) + min)
}

const findTaskByName = (newTask: TTask) => {
    return tasks.find((task) => task.name === newTask.name)
}

const findTaskById = (id: number) => {
    return tasks.find((task) => task.id === id)
}

const findTaskIndexById = (id: number) => {
    return tasks.findIndex((task) => task.id === id)
}

export function addTask(task: TTask) {
    const taskExists = findTaskByName(task)
    if (taskExists) {
        throw new Error('Task with name, already exists')
    }

    const taskId = task.id ? task.id : generateTaskId(1, 1000)
    task.id = taskId
    tasks.push(task)

    return {
        message: 'New Task added successfully',
        code: 201,
        status: "success",
        data: null,
    }
}

export function getTasks() {
    return {
        message: 'Tasks fetched successfully',
        code: 200,
        status: "success",
        data: tasks,
    }
}

export function fetchTaskById(id: number) {
    let task = findTaskById(id)
    if (!task) {
        throw new Error('Task with id, not found')
    }

    return {
        message: 'Task fetched successfully',
        code: 200,
        status: "success",
        data: task,
    }
}

export function editTask(id: number, newTask: TTask) {
    let task = findTaskById(id)
    if (!task) {
        throw new Error('Task with id, not found')
    }

    const updatedTasks = tasks.map((task) => {
        let updatedTask = { ...task }
        if (task.id == id) {
            updatedTask = {
                ...updatedTask,
                ...newTask
            }
        }
        return updatedTask
    })

    tasks = updatedTasks

    return {
        message: 'Task updated successfully',
        code: 200,
        status: "success",
        data: tasks,
    }
}

export function deleteTaskById(id: number) {
    let taskIndex = findTaskIndexById(id)
    if (taskIndex < 0) {
        throw new Error('Task with id, not found')
    }

    tasks.splice(taskIndex, 1)

    return {
        message: 'Task deleted successfully',
        code: 200,
        status: "success",
        data: tasks,
    }
}