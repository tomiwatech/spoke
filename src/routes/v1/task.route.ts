import { addNewTask, deleteTask, getAllTasks, getTaskById, updateTask } from 'controllers/task.controller';
import { Router } from 'express';
import { validateNewTask, validateParam, validateUpdateNewTask } from 'middleware/validation/task.validator';

const router = Router();

router.post('', validateNewTask, addNewTask);

router.get('', getAllTasks);

router.get('/:id', validateParam, getTaskById);

router.delete('/:id', validateParam,  deleteTask);

router.put('/:id', validateParam, validateUpdateNewTask, updateTask);


export default router;
