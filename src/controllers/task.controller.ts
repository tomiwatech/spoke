import { Request, Response, NextFunction } from 'express';
import { addTask, deleteTaskById, editTask, fetchTaskById, getTasks } from 'services/task.service';
import { CustomError } from 'utils/response/custom-error/customer-error';

export const addNewTask = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const response = addTask(req.body)
        res.status(200).json(response)
    } catch (err) {
        const customError = new CustomError(400, 'Raw', 'Error', null, err);
        return next(customError);
    }
};

export const getAllTasks = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const response = getTasks()
        res.status(200).json(response)
    } catch (err) {
        const customError = new CustomError(400, 'Raw', 'Error', null, err);
        return next(customError);
    }
};


export const getTaskById = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const { id } = req.params
        const response = fetchTaskById(+id)
        res.status(200).json(response)
    } catch (err) {
        const customError = new CustomError(400, 'Raw', 'Error', null, err);
        return next(customError);
    }
};


export const updateTask = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const { id } = req.params
        const response = editTask(+id, req.body)
        res.status(200).json(response)
    } catch (err) {
        const customError = new CustomError(400, 'Raw', 'Error', null, err);
        return next(customError);
    }
};


export const deleteTask = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const { id } = req.params
        const response = deleteTaskById(+id)
        res.status(200).json(response)
    } catch (err) {
        const customError = new CustomError(400, 'Raw', 'Error', null, err);
        return next(customError);
    }
};
